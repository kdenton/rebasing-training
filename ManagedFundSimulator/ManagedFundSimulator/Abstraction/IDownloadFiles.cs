﻿namespace ManagedFundSimulator.Abstraction
{
    public interface IDownloadFiles
    {
        string DownloadFileAndSaveInMemory();
    }
}