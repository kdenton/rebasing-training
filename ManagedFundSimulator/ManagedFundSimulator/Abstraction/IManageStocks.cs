﻿using System.Collections.Generic;
using ManagedFundSimulator.Model;

namespace ManagedFundSimulator.Abstraction
{
    public interface IManageStocks
    {
        List<Stock> ConvertFileToStockList(string stockFile);
        List<Stock> GetTenRandomStocks(List<Stock> stockList);
    }
}