﻿using System.Collections.Generic;
using ManagedFundSimulator.Model;

namespace ManagedFundSimulator.Abstraction
{
    public interface IRetrieveStockList
    {
        List<Stock> RetrieveStockList();
    }
}