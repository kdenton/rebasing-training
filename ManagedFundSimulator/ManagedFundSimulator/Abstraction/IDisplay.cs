﻿using System.Collections.Generic;
using ManagedFundSimulator.Model;

namespace ManagedFundSimulator.Abstraction
{
    public interface IDisplay
    {
        void DisplayStocks(List<Stock> stockList);
        void DisplayClosingMessage();
    }
}