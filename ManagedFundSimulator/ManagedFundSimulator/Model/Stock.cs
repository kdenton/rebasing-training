﻿namespace ManagedFundSimulator.Model
{
    public class Stock
    {
        public string Symbol { get; set; }
        public string SecurityName { get; set; }
    }
}