﻿using ManagedFundSimulator.Abstraction;

namespace ManagedFundSimulator
{
    public class AppRunner
    {
        private readonly IRetrieveStockList _stockRetriever;
        private readonly IDisplay _display;

        public AppRunner(IRetrieveStockList stockRetriever, IDisplay display)
        {
            _stockRetriever = stockRetriever;
            _display = display;
        }
        
        public void Run()
        {
            _display.DisplayStocks(_stockRetriever.RetrieveStockList());
            _display.DisplayClosingMessage();
        }
    }
}