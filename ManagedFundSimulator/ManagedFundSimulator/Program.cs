﻿using ManagedFundSimulator.Implementation;

namespace ManagedFundSimulator
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var downloader = new NasdaqTraderFileDownloader();
            var stockManager = new StockManager();
            var retriever = new StockListRetriever(downloader, stockManager);
            var display = new DisplayOnConsole();
            
            var appRunner = new AppRunner(retriever, display);
            
            appRunner.Run();
        }
    }
}