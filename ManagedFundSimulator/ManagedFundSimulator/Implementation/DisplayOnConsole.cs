﻿using System;
using System.Collections.Generic;
using ManagedFundSimulator.Abstraction;
using ManagedFundSimulator.Model;

namespace ManagedFundSimulator.Implementation
{
    public class DisplayOnConsole : IDisplay
    {
        public void DisplayStocks(List<Stock> stockList)
        {
            stockList.ForEach(x =>
            {
                Console.WriteLine(x.Symbol + " | " + x.SecurityName);
            });
        }

        public void DisplayClosingMessage()
        {
            Console.WriteLine("Thanks for using our program!");
        }
    }
}