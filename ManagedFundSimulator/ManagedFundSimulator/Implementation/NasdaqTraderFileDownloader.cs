﻿using System.IO;
using System.Net;
using System.Text;
using ManagedFundSimulator.Abstraction;

namespace ManagedFundSimulator.Implementation
{
    public class NasdaqTraderFileDownloader : IDownloadFiles
    {
        public string DownloadFileAndSaveInMemory()
        {
            var request = WebRequest.Create("ftp://ftp.nasdaqtrader.com/SymbolDirectory/nasdaqlisted.txt");

            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null) return string.Empty;
                    
                    var reader = new StreamReader(stream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
        }
    }
}