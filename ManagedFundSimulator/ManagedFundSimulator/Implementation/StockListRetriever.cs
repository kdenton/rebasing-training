﻿using System;
using System.Collections.Generic;
using System.Linq;
using ManagedFundSimulator.Abstraction;
using ManagedFundSimulator.Model;

namespace ManagedFundSimulator.Implementation
{
    public class StockListRetriever : IRetrieveStockList
    {
        private readonly IDownloadFiles _fileDownloader;
        private readonly IManageStocks _stockManager;

        public StockListRetriever(IDownloadFiles fileDownloader, IManageStocks stockManager)
        {
            _fileDownloader = fileDownloader;
            _stockManager = stockManager;
        }
        
        public List<Stock> RetrieveStockList()
        {
            var rawStockList = _fileDownloader.DownloadFileAndSaveInMemory();

            return _stockManager.GetTenRandomStocks(_stockManager.ConvertFileToStockList(rawStockList));
        }
    }
}