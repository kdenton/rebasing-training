﻿using System;
using System.Collections.Generic;
using System.Linq;
using ManagedFundSimulator.Abstraction;
using ManagedFundSimulator.Model;

namespace ManagedFundSimulator.Implementation
{
    public class StockManager : IManageStocks
    {
        public List<Stock> ConvertFileToStockList(string stockFile)
        {
            var rawStockList = stockFile.Split(new[] {Environment.NewLine}, StringSplitOptions.None)
                .Select(x => x.Split('|'))
                .ToList();
            
            rawStockList.RemoveRange(rawStockList.Count - 2, 2);
            
            return rawStockList.Select(stock => new Stock
                {
                    Symbol = stock[0],
                    SecurityName = stock[1]
                })
                .Where(x => !x.SecurityName.ToLower().Contains("test"))
                .ToList();
        }

        public List<Stock> GetTenRandomStocks(List<Stock> stockList)
        {
            var shuffled = stockList.OrderBy(a => Guid.NewGuid()).ToList();

            var numToTake = 10;

            if (stockList.Count < 10)
            {
                numToTake = stockList.Count;
            }
            
            return shuffled.Take(numToTake).ToList();
        }
    }
}