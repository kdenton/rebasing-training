﻿using System;
using System.Collections.Generic;
using System.IO;
using ManagedFundSimulator.Implementation;
using ManagedFundSimulator.Model;
using Xunit;

namespace ManagedFundSimulator.Unit_Tests.Implementation
{
    public class DisplayOnConsoleTest
    {
        [Fact]
        public void DisplayStocks_GivenAListOfStocks_ItPrintsThemToTheConsole()
        {
            var stockList = new List<Stock>
            {
                new Stock
                {
                    SecurityName = "Test 1",
                    Symbol = "TEST1"
                },
                new Stock
                {
                    SecurityName = "Test 2",
                    Symbol = "TEST2"
                }
            };
            var expectedOutput = stockList[0].Symbol + " | " + stockList[0].SecurityName + Environment.NewLine
                                 + stockList[1].Symbol + " | " + stockList[1].SecurityName + Environment.NewLine;
            
            using (var writer = new StringWriter())
            {
                Console.SetOut(writer);
                
                var display = new DisplayOnConsole();
                
                display.DisplayStocks(stockList);
                
                Assert.Equal(expectedOutput, writer.ToString());
            }
        }

        [Fact]
        public void DisplayClosingMessaage_PrintsClosingMessageToConsole()
        {
            var expectedOutput = "Thanks for using our program!" + Environment.NewLine;
            using (var writer = new StringWriter())
            {
                Console.SetOut(writer);
                
                var display = new DisplayOnConsole();
                
                display.DisplayClosingMessage();
                
                Assert.Equal(expectedOutput, writer.ToString());
            }
        }
    }
}