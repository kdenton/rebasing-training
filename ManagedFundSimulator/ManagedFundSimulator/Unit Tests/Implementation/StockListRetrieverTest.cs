﻿using System;
using System.Collections.Generic;
using ManagedFundSimulator.Abstraction;
using ManagedFundSimulator.Implementation;
using ManagedFundSimulator.Model;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace ManagedFundSimulator.Unit_Tests.Implementation
{
    public class StockListRetrieverTest
    {
        [Fact]
        public void RetrieveStockList_GivenDownloadedFile_ItConvertsThenReturnsListOfStock()
        {
            var file = "STOCK1|Stock One|S|N|N|100|N|N" + Environment.NewLine +
                       "STOCK2|Stock Two|S|N|N|100|N|N" + Environment.NewLine +
                       "STOCK3|Stock Three|S|N|N|100|N|N" + Environment.NewLine +
                       "TEST1|Test One|S|N|N|100|N|N" + Environment.NewLine;
            
            var returnedStockList = new List<Stock>
            {
                new Stock
                {
                    Symbol = "STOCK1",
                    SecurityName = "Stock One"
                },
                new Stock
                {
                    Symbol = "STOCK2",
                    SecurityName = "Stock Two"
                },
                new Stock
                {
                    Symbol = "STOCK3",
                    SecurityName = "Stock Three"
                }
            };
            
            var downloadMock = new Mock<IDownloadFiles>();
            var stockManagerMock = new Mock<IManageStocks>();
            downloadMock.Setup(x => x.DownloadFileAndSaveInMemory()).Returns(file);
            stockManagerMock.Setup(x => x.ConvertFileToStockList(It.IsAny<string>())).Returns(returnedStockList);
            stockManagerMock.Setup(x => x.GetTenRandomStocks(It.IsAny<List<Stock>>())).Returns(returnedStockList);
            var retriever = new StockListRetriever(downloadMock.Object, stockManagerMock.Object);
            var randomStockList = retriever.RetrieveStockList();
            
            Assert.Equal(3, randomStockList.Count);
            downloadMock.Verify(x => x.DownloadFileAndSaveInMemory(), Times.Once);
        }
    }
}