﻿using System;
using System.Collections.Generic;
using ManagedFundSimulator.Implementation;
using ManagedFundSimulator.Model;
using Newtonsoft.Json;
using Xunit;

namespace ManagedFundSimulator.Unit_Tests.Implementation
{
    public class StockManagerTest
    {
        [Fact]
        public void ConvertFileToStockList_GivenAFileInProperFormat_ItConvertsToListOfStock()
        {
            var file = "STOCK1|Stock One|S|N|N|100|N|N" + Environment.NewLine +
                       "STOCK2|Stock Two|S|N|N|100|N|N" + Environment.NewLine +
                       "STOCK3|Stock Three|S|N|N|100|N|N" + Environment.NewLine +
                       "TEST1|Test One|S|N|N|100|N|N" + Environment.NewLine;
            var expectedStockList = JsonConvert.SerializeObject(new List<Stock>
            {
                new Stock
                {
                    Symbol = "STOCK1",
                    SecurityName = "Stock One"
                },
                new Stock
                {
                    Symbol = "STOCK2",
                    SecurityName = "Stock Two"
                },
                new Stock
                {
                    Symbol = "STOCK3",
                    SecurityName = "Stock Three"
                }
            });
            
            var stockManager = new StockManager();
            
            Assert.Equal(expectedStockList, JsonConvert.SerializeObject(stockManager.ConvertFileToStockList(file)));
        }
    }
}